class Api::V1::DisbursementsController < ApplicationController

    def get_by_week

        merchant_id = params[:merchant_id].blank? ? nil : params[:merchant_id]
        date = params[:date].to_datetime

        rows = Disbursement.get_by_week(date, merchant_id)

        return render status:200, json: rows.map{ |row| {
            merchant_id: row.merchant_id,
            disbursed_amount: row.disbursed_amount.to_f
        }}
    end

end
