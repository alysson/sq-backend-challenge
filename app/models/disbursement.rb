class Disbursement < ApplicationRecord
  belongs_to :merchant

  def self.create_from_date(date, merchant_id)
    date = date.to_date if date.is_a?(String)
    row = Disbursement.new
    row.week_start_date = date.beginning_of_week
    row.week_end_date = date.end_of_week
    row.merchant_id = merchant_id
    row.week_number = date.strftime('%-V')
    row.save
  end

  def self.generate_by_week(date, merchant_id = nil)

    date = date.to_datetime if date.is_a?(String)

    rows = Order.group(:merchant_id).where(completed_at: date.all_week)

    if ! merchant_id.blank?
      rows = rows.where(merchant: merchant_id)
    end
    
    rows = rows.pluck(Arel.sql("merchant_id, SUM(amount) - SUM(fee)"))

    arr_disbursements = []

    rows.each do |row|
      arr_disbursements.push({
        merchant_id: row[0],
        disbursed_amount: row[1].to_f,
        week_start_date: date.beginning_of_week,
        week_end_date: date.end_of_week,
        week_number: date.strftime('%-V').to_i
      })
    end

    Disbursement.insert_all(arr_disbursements) if arr_disbursements.length > 0

  end

  def self.get_by_week(date, merchant_id = nil)
    date = date.to_datetime if date.is_a?(String)

    rows = Disbursement.where(week_start_date: date.beginning_of_week, week_end_date: date.end_of_week)

    if ! merchant_id.blank?
      rows = rows.where(merchant: merchant_id)
    end

    rows.select(:merchant_id, :disbursed_amount)

  end

end
