class Order < ApplicationRecord
  attr_accessor :real_fee
  belongs_to :merchant
  belongs_to :shopper

  #https://github.com/rails/rails/pull/38148
  #https://ruby-doc.org/stdlib-3.1.2/libdoc/bigdecimal/rdoc/BigDecimal.html#method-c-mode
  def self.calc_fee(row)
    row.raw_fee = row.amount * row.fee_per_order
    row.fee = ActiveSupport::NumberHelper.number_to_rounded(row.raw_fee, precision: 2, round_mode: :banker)
    row.save
  end

  def fee_per_order
    return FEE_PER_ORDER_RANGE_GT300 if amount > 300.to_f
    return FEE_PER_ORDER_RANGE_GT50LT300 if amount > 50.to_f
    return FEE_PER_ORDER_RANGE_LT50
  end

end
