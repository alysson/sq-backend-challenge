class CreateDisbursementsJob < ApplicationJob
  queue_as :create_disbursements

  def perform(*args)
    d = Time.now.yesterday
    Disbursement.generate_by_week(d)
  end
end
