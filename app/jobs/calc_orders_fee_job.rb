class CalcOrdersFeeJob < ApplicationJob
  queue_as :disbursements

  def perform(limit=10)
    return false unless limit.to_i > 0
    get_rows(limit).each do |row|
      Order.calc_fee(row)
    end
  end

  def get_rows(limit)
    Order.where(raw_fee: nil).where.not(completed_at: nil).order("completed_at ASC").limit(limit)
  end
end
