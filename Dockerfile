FROM ruby:3.0
RUN mkdir /app
WORKDIR /app
COPY Gemfile /app/Gemfile
COPY Gemfile.lock /app/Gemfile.lock
RUN gem install rails
RUN gem install bundler
RUN bundle install
COPY . /app
