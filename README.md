##### Prerequisites

The setups steps expect following tools installed on the system.

- Ruby [3.0.4](https://gitlab.com/alysson/sq-backend-challenge/-/blob/master/.ruby-version)
- Rails [7.0.3](https://gitlab.com/alysson/sq-backend-challenge/-/blob/master/Gemfile#L7)

##### 1. Check out the repository

```bash
git clone git@gitlab.com:alysson/sq-backend-challenge.git
```

###### 1.1. Optionally: set up environment with Docker
see [docker-composer.yml](https://gitlab.com/alysson/sq-backend-challenge/-/blob/master/docker-compose.yml) and [Dockerfile](https://gitlab.com/alysson/sq-backend-challenge/-/blob/master/Dockerfile)

##### 2. Set database configuration

Copy the sample .env.template file and edit the database configuration as required.

```bash
cp .env.template .env
```

##### 3. Create and setup the database

Run the following commands to create and setup the database.

```ruby
bundle exec rake db:create
bundle exec rake db:setup
```

##### 4. Parametrize

file: [config/initializers/my_constants.rb](https://gitlab.com/alysson/sq-backend-challenge/-/blob/master/config/initializers/my_constants.rb)

```ruby
FEE_PER_ORDER_RANGE_LT50=0.01
FEE_PER_ORDER_RANGE_GT50LT300=0.0095
FEE_PER_ORDER_RANGE_GT300=0.0085
```

see [Disbursement model](https://gitlab.com/alysson/sq-backend-challenge/-/blob/master/app/models/disbursement.rb)

##### 5. Start the Rails server

You can start the rails server using the command given below.

```ruby
bundle exec rails s
```

And now you can visit the site with the URL http://localhost:3000

##### 6. Background processes to calc fee and disbursements

```ruby
# calc and fill Order's fee
# for future orders, it should be calculated after order set as completed
# v2 should include a microservice to handle this operations
CalcOrdersFeeJob.perform_now(n) #will run calc for the first n lines
```

```ruby
#fill Disbursement's table
CreateDisbursementsJob.perform_now() #will generate for past week (run every Monday)
```

##### 7. API Docs

_Swagger Docs_

[http://localhost:3000/api-docs/index.html](http://localhost:3000/api-docs/index.html)

##### 8. Authentication

_TODO_

##### 9. Dump.sql
_this kind of file should not be in the repository) - adding just for the test/challenge review/check the data/results_

[dump.sql](https://gitlab.com/alysson/sq-backend-challenge/-/blob/master/dump.sql)
