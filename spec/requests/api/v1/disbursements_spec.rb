require 'swagger_helper'

RSpec.describe 'api/v1/disbursements', type: :request do

  path '/api/v1/disbursements/{date(}/{merchant_id)}' do
    # You'll want to customize the parameter types...
    parameter name: 'date', in: :path, type: :string, description: 'date'
    parameter name: 'merchant_id', in: :path, type: :string, description: 'merchant_id'

    get('get_by_week disbursement') do
      response(200, 'successful') do
        let(:date) { '123' }
        let(:merchant_id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end
end
