class AddRawFeeToOrders < ActiveRecord::Migration[7.0]
  def change
    add_column :orders, :raw_fee, :decimal
  end
end
