class AddIndexToDisbursements < ActiveRecord::Migration[7.0]
  def change
    add_index :disbursements, [:week_start_date, :week_end_date, :merchant_id], unique: true, name: 'pk_disbursements'
  end
end
