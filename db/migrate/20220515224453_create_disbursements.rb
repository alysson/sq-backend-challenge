class CreateDisbursements < ActiveRecord::Migration[7.0]
  def change
    create_table :disbursements, :id => false do |t|
      t.datetime :week_start_date
      t.datetime :week_end_date
      t.string :week_number
      t.references :merchant, null: false, foreign_key: true
      t.decimal :disbursed_amount

      t.timestamps
    end
  end
end
